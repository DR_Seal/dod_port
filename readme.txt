Disclaimer:
Included within this project is the game files and source code required to compile and run Day of Defeat Source on Source SDK Base 2013.
If you do not have Day of Defeat Source installed you will encounter errors with the mounting process.
I'm not responsible for any damages caused from running or compiling this port nor am I affiliated with Valve.
Valve holds all rights to the engine code, game code, game assets and any IPs used in this port.